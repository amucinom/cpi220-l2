package org.gjt.sp.jedit.cpi220.performance;

import org.gjt.sp.util.Log;
import java.lang.instrument.Instrumentation;

public class SpaceCalculator 
{
	    private static Instrumentation instrumentation;

	    public static void premain(String args, Instrumentation inst) {
	        instrumentation = inst;
	    }

	    public static long getObjectSize(Object o) {
	        return instrumentation.getObjectSize(o);
	    }
}
