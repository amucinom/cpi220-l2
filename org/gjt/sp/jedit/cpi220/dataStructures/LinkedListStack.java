package org.gjt.sp.jedit.cpi220.dataStructures;

import java.util.NoSuchElementException;
import java.util.Iterator;
import org.gjt.sp.jedit.cpi220.performance.SpaceCalculator;
import org.gjt.sp.util.Log;

public class LinkedListStack<Item> implements Iterable<Item>, IStack<Item>
{

/**
 *  The <tt>Stack</tt> class represents a last-in-first-out (LIFO) stack of generic items.
 *  It supports the usual <em>push</em> and <em>pop</em> operations, along with methods
 *  for peeking at the top item, testing if the stack is empty, and iterating through
 *  the items in LIFO order.
 *  <p>
 *  This implementation uses a singly-linked list with a static nested class for
 *  linked-list nodes. See {@link LinkedStack} for the version from the
 *  textbook that uses a non-static nested class.
 *  The <em>push</em>, <em>pop</em>, <em>peek</em>, <em>size</em>, and <em>is-empty</em>
 *  operations all take constant time in the worst case.
 *  <p>
 *  For additional documentation, see <a href="/algs4/13stacks">Section 1.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *
 *  @param <Item> the generic type of an item in this stack
 */
    private int N;                // size of the stack
    private Node<Item> first;     // top of stack

    // helper linked list class
    private static class Node<Item> {
        private Item item;
        private Node<Item> next;
    }

    /**
     * Initializes an empty stack.
     */
    public LinkedListStack() {
        first = null;
        N = 0;
    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.dataStructures.IStack#isEmpty()
	 */
    @Override
	public boolean isEmpty() {
        return first == null;
    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.dataStructures.IStack#size()
	 */
    @Override
	public int size() {
        return N;
    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.dataStructures.IStack#push(Item)
	 */
    @Override
	public void push(Item item) {
    	SpaceCalculator sc = new SpaceCalculator();
        Node<Item> oldfirst = first;
        first = new Node<Item>();
        first.item = item;
        first.next = oldfirst;
        N++;
        
    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.dataStructures.IStack#pop()
	 */
    @Override
	public Item pop() {
        if (isEmpty()) 
        	return null;
        else
        {//throw new NoSuchElementException("Stack underflow");
	        Item item = first.item;        // save item to return
	        first = first.next;            // delete first node
	        N--;
	        return item;                   // return the saved item
        }
    }


    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.dataStructures.IStack#peek()
	 */
    @Override
	public Item peek() {
        //if (isEmpty()) throw new NoSuchElementException("Stack underflow");
    	if (isEmpty()) 
    		return null;
    	else
    		return first.item;
    }
    

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.dataStructures.IStack#iterator()
	 */
    @Override
	public Iterator<Item> iterator() {
        return new ListIterator<Item>(first);
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator<Item> implements Iterator<Item> {
        private Node<Item> current;
        private Node<Item> prev;

        public ListIterator(Node<Item> first) {
            current = first;
            prev = first;
        }

        public boolean hasNext() {
            return current.next != null;
        }

        public void remove() {
        	Log.log(1,this,"remove" + (prev == null) + (current == null));
            prev.next = current.next;
            current = prev;
            N--;
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            prev = current;
            current = current.next; 
            return item;
        }
    }



}
