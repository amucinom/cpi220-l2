package org.gjt.sp.jedit.cpi220.dataStructures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public interface IStack<Item> {

	/**
	 * Returns true if this stack is empty.
	 *
	 * @return true if this stack is empty; false otherwise
	 */
	boolean isEmpty();

	/**
	 * Returns the number of items in this stack.
	 *
	 * @return the number of items in this stack
	 */
	int size();

	/**
	 * Adds the item to this stack.
	 *
	 * @param  item the item to add
	 */
	void push(Item item);

	/**
	 * Removes and returns the item most recently added to this stack.
	 *
	 * @return the item most recently added
	 * @throws NoSuchElementException if this stack is empty
	 */
	Item pop();

	/**
	 * Returns (but does not remove) the item most recently added to this stack.
	 *
	 * @return the item most recently added to this stack
	 * @throws NoSuchElementException if this stack is empty
	 */
	Item peek();

	/**
	 * Returns an iterator to this stack that iterates through the items in LIFO order.
	 *
	 * @return an iterator to this stack that iterates through the items in LIFO order
	 */
	Iterator<Item> iterator();

}